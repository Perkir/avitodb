using System;
using AvitoModels;
using System.Reflection;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Data.SQLite;
//using Microsoft.Data.Sqlite;

namespace ADOCoreAdapter
{
    public class Repository: IRepository
    {
        private readonly SQLiteConnection _connection;

        public Repository(string connectionString)
        {
            _connection = new SQLiteConnection(connectionString);
            _connection.Open();
        }
        
        #region CRUD
        
        public void Create<T>(T entity) where T : Entity
        {
           
            var (f,v) = FieldsAndValues(typeof(T));
            var cmdString = $"INSERT INTO {TableName(typeof(T))} ({f}) VALUES ({v});" +
                            $" SELECT last_insert_rowid();";
            using var cmd = new SQLiteCommand(cmdString, _connection);
            foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                if (field != null && field != "Id")
                {
                    cmd.Parameters.AddWithValue(field, prop.GetValue(entity));
                }
            }
            entity.Id = (long) cmd.ExecuteScalar();
        }

        public T Read<T>(long id) where T : Entity, new()
        {
            T entity;
            var cmdString = $"SELECT * FROM {TableName(typeof(T))} WHERE Id = @id";
            using (var cmd = new SQLiteCommand(cmdString, _connection))
            {
                cmd.Parameters.AddWithValue("id", id);
                using var reader = cmd.ExecuteReader();
                if (!reader.Read()) return null;
                entity = new T();
                foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    string field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                    if (field != null)
                    {
                        prop.SetValue(entity, reader.GetValue(reader.GetOrdinal(field)));
                    }
                }
            }
            return entity;

        }

        public void Delete<T>(T entity) where T : Entity
        {
            var cmdString = $"DELETE FROM {TableName(typeof(T))} WHERE Id = @id";
            using var cmd = new SQLiteCommand(cmdString, _connection);
            cmd.Parameters.AddWithValue("id", entity.Id);
            cmd.ExecuteNonQuery();
        }

        public void Update<T>(T entity) where T : Entity
        {
            var (f,_) = FieldsAndValues(typeof(T), independent: false);
            var cmdString = $"UPDATE {TableName(typeof(T))} SET {f} WHERE Id = @id";
            using var cmd = new SQLiteCommand(cmdString, _connection);
            cmd.Parameters.AddWithValue("id", entity.Id);
            foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                if (field != null && field != "Id")
                {
                    cmd.Parameters.AddWithValue(field, prop.GetValue(entity));
                }
            }
            cmd.ExecuteNonQuery();
        }
        
        #endregion CRUD  

        public User AddUser(string firstName, string lastName, DateTime birthdate)
        {
            var user = new User { FirstName = firstName, LastName = lastName, BirthDate = birthdate};
            Create<User>(user);
            return user;
        }

        public void AddUserInfo(User user, string phone, string email)
        {
            var userInfo = new UserInfo { UserId = user.Id, Phone = phone, Email = email};
            Create<UserInfo>(userInfo);
        }

        public Advert GetAdvertByNumber(string number)
        {
            Advert advert = null;
            const string cmdString = "SELECT * FROM adverts WHERE number = @number";
            using (var cmd = new SQLiteCommand(cmdString, _connection))
            {
                cmd.Parameters.AddWithValue("number", number);
                using var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    advert = new Advert
                    {
                        Id = reader.GetFieldValue<long>(reader.GetOrdinal("Id")),
                        Created = Convert.ToDateTime(reader.GetFieldValue<string>(reader.GetOrdinal("Created"))),
                        Number = reader.GetFieldValue<string>(reader.GetOrdinal("Number")),
                        Text = reader.GetFieldValue<string>(reader.GetOrdinal("Text")),
                        UserId = reader.GetFieldValue<long>(reader.GetOrdinal("UserId"))
                    };
                }
            }
            return advert;
        }

        public Advert AddAdvert(DateTime created, string number, string text, User user)
        {
            var advert = new Advert { Created = created, Number = number, Text = text, UserId = user.Id };
            Create<Advert>(advert);
            return advert;
        }

        public void AddHistory(DateTime created, string comment, HistoryType type, Advert advert)
        {
            var history = new History { Created = created, Comment = comment, Type = type, AdvertId = advert.Id};
            Create<History>(history);
        }
        
        public void Dispose()
        {
            _connection.Close();
        }
        
        private static string TableName(MemberInfo t)
        {
            return t.GetCustomAttribute<TableAttribute>()?.Name;
        }
        private (string, string) FieldsAndValues(IReflect t, bool withoutId = true, bool independent = true)
        {
            var props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var fields = new StringBuilder();
            var values = new StringBuilder();
            foreach (var prop in props)
            {
                var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                switch (field)
                {
                    case "Id" when withoutId:
                    case null:
                        continue;
                }

                if (fields.Length > 0)
                {
                    fields.Append(',');
                    if (independent)
                    {
                        values.Append(',');
                    }
                }
                fields.Append(field);
                if (independent)
                {
                    values.Append("@" + field);
                }
                else
                {
                    fields.Append(" = @" + field);
                }
            }
            return (fields.ToString(), values.ToString());
        }
        
    }
}