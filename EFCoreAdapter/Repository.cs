using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AvitoModels;

namespace EFCoreAdapter
{
    public class Repository : IRepository
    {
        private DataContext context;
        
        public Repository(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            var options = optionsBuilder.UseSqlite(connectionString).Options;

            context = new DataContext(options);
        }
      
#region CRUD
        public void Create<T>(T entity) where T : Entity
        {
            var entitySet = context.Set<T>();
            entitySet.Add(entity);
            context.SaveChanges();
        }

        public T Read<T>(long id) where T : Entity, new()
        {
            var entitySet = context.Set<T>();
            return entitySet
                .SingleOrDefault(e => e.Id == id);
        }

        public void Delete<T>(T entity) where T : Entity
        {
            var entitySet = context.Set<T>();
            entitySet.Remove(entity);
            context.SaveChanges();
        }

        public void Update<T>(T entity) where T : Entity
        {
            var entitySet = context.Set<T>();
            entitySet.Update(entity);
            context.SaveChanges();
        }
#endregion        

        public User AddUser(string firstName, string lastName, DateTime birthdate)
        {
            User user = new User { FirstName = firstName, LastName = lastName, BirthDate = birthdate};
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }

        public void AddUserInfo(User user, string phone, string email)
        {
            context.UserInfos.Add(new UserInfo { Phone = phone, Email = email, User = user });
            context.SaveChanges();

        }

        public Advert GetAdvertByNumber(string number)
        {
            return context.Adverts.FirstOrDefault(a => a.Number == number);
        }

        public Advert AddAdvert(DateTime created, string number, string text, User user)
        {
            Advert advert = new Advert {Created = created, Number = number, Text = text, User = user};
            context.Adverts.Add(advert);
            context.SaveChanges();

            return advert;
        }

        public void AddHistory(DateTime created, string comment, HistoryType type, Advert advert)
        {
            context.Histories.Add(new History{Created = created, Comment = comment, Type = type, Advert = advert});
            context.SaveChanges();
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}