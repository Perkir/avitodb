using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoModels
{
    public interface IRepository : IDisposable
        {
            void Create<T>(T entity) where T : Entity;
            T Read<T>(long id) where T : Entity, new();
            void Delete<T>(T entity) where T : Entity;
            void Update<T>(T entity) where T : Entity;

            User AddUser(string firstName, string lastName, DateTime birthdate);
            void AddUserInfo(User user, string phone, string email);
            Advert GetAdvertByNumber(string number);

            Advert AddAdvert(DateTime created, string number, string text, User user);

            void AddHistory(DateTime created, string comment, HistoryType type, Advert advert);

        }
}