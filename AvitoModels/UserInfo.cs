using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoModels
{
    [Table("UserInfos")]
    public class UserInfo : Entity
    {
        [Column("Phone")] public string Phone { get; set; }
        [Column("Email")] public string Email { get; set; }
        [Column("UserId")] public long UserId { get; set; }
        public User User { get; set; }

    }
}