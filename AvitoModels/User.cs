using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoModels
{
    // Пользователи
    [Table("users")]
    public class User: Entity
    {
        [Column("FirstName")] public string FirstName { get; set; }
        [Column("LastName")] public string LastName { get; set; }
        [Column("Birthdate")] public DateTime BirthDate { get; set; }
        public List<UserInfo> UserInfos { get; set; }
        public List<Advert> Adverts { get; set; }
        
        public override string ToString()
        {
            return $"{{{FirstName} {LastName} {BirthDate}}}";
        }

    }
}