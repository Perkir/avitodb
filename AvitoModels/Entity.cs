using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoModels
{
    public abstract class Entity
    {
        [Column("Id")]
        public long Id { get; set; }
    }

}