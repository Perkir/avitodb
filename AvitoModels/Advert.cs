using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoModels
{
    // Объявления
    [Table("adverts")]
    public class Advert: Entity
    {
        [Column("Created")] public DateTime Created { get; set; }   
        [Column("Number")] public string Number { get; set; }
        [Column("Text")] public string Text { get; set; }
        
        [Column("UserId")] public long UserId { get; set; }
        public User User { get; set; }
    }
}