using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoModels
{
    public enum HistoryType
    {
        Create,
        Delete,   
        Modify   
    }
    //История обьявлений
    [Table("Histories")]
    public class History: Entity
    {
        [Column("Created")]public DateTime Created { get; set; }
        [Column("Comment")]public string Comment { get; set; }
        [Column("Type")] public HistoryType Type { get; set; }

        [Column("AdvertId")] public long AdvertId { get; set; }
        public Advert Advert { get; set; }

    }
}