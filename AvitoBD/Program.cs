﻿using System;
using System.IO;
using AvitoModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;


using EFCoreAdapter;
//using ADOCoreAdapter;

namespace AvitoBD
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .Build();
            var connectionString = builder.GetConnectionString("AvitoContext");

            using IRepository repo = new Repository(connectionString);
            // Создаем пользователей
            var user0 = repo.AddUser("Иван", "Иванов", DateTime.Parse("04.04.1990"));
            repo.AddUserInfo(user0, "+78888888", "Ivanov@ivan.ivan");
            var user1 = repo.AddUser("Петя", "Петров", DateTime.Parse("05.05.1990"));
            repo.AddUserInfo(user1, "+79999999", "Petrov@petr.petr");

            //Создаем объявления
            var advert0 = repo.AddAdvert(DateTime.Now, "0", "Продам слона", user0);
            repo.AddHistory(DateTime.Now, "Создано объявление о продаже слона", HistoryType.Create, advert0);
            var advert1 = repo.AddAdvert(DateTime.Now, "1", "Продам Шоколад", user1);
            repo.AddHistory(DateTime.Now, "Создано объявление о продаже Шоколада", HistoryType.Create, advert1);
            
            //Ищем объявление по номеру
            repo.GetAdvertByNumber(advert0.Number);
            repo.GetAdvertByNumber(advert1.Number);
            
        }
    }
}